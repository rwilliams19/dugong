﻿Imports System.Data.Odbc
Imports System.Data.SqlClient

Public Class frmSelectCol
	Public ConString As String = frmConnect.ConString
	Public ConType As String = frmConnect.ConType
	Public dtype

    Private Sub frmSelectCol_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If frmConnect.prod.ToLower = "crd" Then
            PictureBoxProd.Image = Dugong.My.Resources.Resources.CRD_Logo
        ElseIf frmConnect.prod.ToLower = "mars" Then
            PictureBoxProd.Image = Dugong.My.Resources.Resources.MARS_Logo
        ElseIf frmConnect.prod.ToLower = "pbrs" Then
            PictureBoxProd.Image = Dugong.My.Resources.Resources.PBRS_Logo
        ElseIf frmConnect.prod.ToLower = "sqlrd" Then
            PictureBoxProd.Image = Dugong.My.Resources.Resources.SQLRD_Logo
        End If
        'Fills cmbTable with tables from DB
        Dim SQL As String = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES"
        Dim reader = Nothing
        Dim con = Nothing
        If ConType = "LOCAL" Then

            con = New SqlConnection(ConString)
            con.Open()
            Dim sqlComm As SqlCommand = New SqlCommand(SQL, con)
            reader = sqlComm.ExecuteReader

        Else 'ConType = "ODBC"

            Dim cn As OdbcConnection = New OdbcConnection(ConString)
            con.Open()
            Dim odbcComm As OdbcCommand = New OdbcCommand(SQL, con)
            reader = odbcComm.ExecuteReader

        End If

        While reader.Read()
            cmbTable.Items.Add(reader.GetValue(0))
        End While
        con.Close()
        con.Dispose()
        con = Nothing
    End Sub

    Private Sub cmbTable_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbTable.SelectedIndexChanged
        cmbColumn.Items.Clear()
        cmbWhereCol.Items.Clear()
        Dim SQL As String = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME LIKE '" & cmbTable.Text & "'"
        Dim reader = Nothing
        Dim con = Nothing
        If ConType = "LOCAL" Then

            con = New SqlConnection(ConString)
            con.Open()
            Dim sqlComm As SqlCommand = New SqlCommand(SQL, con)
            reader = sqlComm.ExecuteReader

        Else 'ConType = "ODBC"

            con = New OdbcConnection(ConString)
            con.Open()
            Dim odbcComm As OdbcCommand = New OdbcCommand(SQL, con)
            reader = odbcComm.ExecuteReader

        End If
        While reader.Read()
            cmbColumn.Items.Add(reader.GetValue(0))
            cmbWhereCol.Items.Add(reader.GetValue(0))
        End While
        con.Close()
        con.Dispose()
        con = Nothing
    End Sub

    Private Sub cmbColumn_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbColumn.SelectedIndexChanged
        Dim Sql As String = "SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME LIKE '" & cmbColumn.Text & "' AND TABLE_NAME LIKE '" & cmbTable.Text & "'"
        Dim reader = Nothing
        Dim con = Nothing
        If ConType = "LOCAL" Then

            con = New SqlConnection(ConString)
            con.Open()
            Dim sqlComm As SqlCommand = New SqlCommand(Sql, con)
            reader = sqlComm.ExecuteReader

        Else 'ConType = "ODBC"

            con = New OdbcConnection(ConString)
            con.Open()
            Dim odbcComm As OdbcCommand = New OdbcCommand(Sql, con)
            reader = odbcComm.ExecuteReader

        End If
        While reader.Read()
            dtype = reader.GetValue(0)
        End While
        con.Close()
        con.Dispose()
        con = Nothing
    End Sub

    Private Sub cmbWhereCol_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbWhereCol.SelectedIndexChanged
        cmbWhereVal.Items.Clear()
        Dim SQL As String = "SELECT DISTINCT CAST(" & cmbWhereCol.Text & " AS VARCHAR(MAX)) FROM " & cmbTable.Text
        Dim reader = Nothing
        Dim con = Nothing
        If ConType = "LOCAL" Then

            con = New SqlConnection(ConString)
            con.Open()
            Dim sqlComm As SqlCommand = New SqlCommand(SQL, con)
            reader = sqlComm.ExecuteReader

        Else 'ConType = "ODBC"

            con = New OdbcConnection(ConString)
            con.Open()
            Dim odbcComm As OdbcCommand = New OdbcCommand(SQL, con)
            reader = odbcComm.ExecuteReader

        End If
        While reader.Read()
            If Not IsDBNull(reader.GetValue(0)) AndAlso reader.getValue(0) <> "" Then
                cmbWhereVal.Items.Add(reader.GetValue(0))
            End If
        End While
        con.Close()
        con.Dispose()
        con = Nothing
    End Sub

    Private Sub Chk_CheckedChanged(sender As Object, e As EventArgs) Handles rbReplace.CheckedChanged, rbOverwrite.CheckedChanged
        If rbOverwrite.Checked Then
            btnVerify.Enabled = True
            rbReplace.Checked = False
            TxtFind.Enabled = False
            chkEncrypt.Enabled = True
            TxtFind.Text = ""
        ElseIf rbReplace.Checked Then
            btnVerify.Enabled = True
            rbOverwrite.Checked = False
            TxtFind.Enabled = True
            chkEncrypt.Enabled = False
            chkEncrypt.Checked = False
        End If
    End Sub

    Private Sub btnVerify_Click(sender As Object, e As EventArgs) Handles btnVerify.Click
        Dim EncryptVal = ""
        If cmbTable.Text = "dugong" Then
            PictureBox2.Visible = False
            PictureBoxProd.Visible = False
            PictureBox1.Enabled = True
            PictureBox1.Visible = True
        End If
        fctbQuery.Text = ""
        Dim qS As New QueryBuilder

        If chkEncrypt.Checked Then
            Dim settingMan As New clsSettingsManager
            EncryptVal = "$$" + settingMan.EncryptString(TxtInsert.Text) + "$$"
        End If

        If rbOverwrite.Checked Then
            If cmbTable.Text = "" Or cmbColumn.Text = "" Or TxtInsert.Text = "" Then
                MessageBox.Show("Please fill out all value boxes before viewing your final query.", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            Else
                Dim OpWhere
                Select Case cmbOperator.Text.ToLower
                    Case "begins with"
                        OpWhere = " LIKE '" & cmbWhereVal.Text & "%'"
                    Case "ends with"
                        OpWhere = " LIKE '%" & cmbWhereVal.Text & "'"
                    Case "contains"
                        OpWhere = " LIKE '%" & cmbWhereVal.Text & "%'"
                    Case "does not contain"
                        OpWhere = " NOT LIKE '%" & cmbWhereVal.Text & "%'"
                    Case "does not begin with"
                        OpWhere = " NOT LIKE '" & cmbWhereVal.Text & "%'"
                    Case "does not end with"
                        OpWhere = " NOT LIKE '%" & cmbWhereVal.Text & "'"
                    Case "is empty"
                        OpWhere = " IS NULL OR " &
                        cmbTable.Text & "." & cmbWhereCol.Text & " ='')"
                    Case "is not empty"
                        OpWhere = " IS NOT NULL AND " &
                        cmbTable.Text & "." & cmbWhereCol.Text & " <> '')"
                    Case Else
                        OpWhere = cmbOperator.Text & " '" & cmbWhereVal.Text & "'"
                End Select
                If EncryptVal <> "" Then
                    qS.AppendString("Update " & cmbTable.Text & " Set " & cmbColumn.Text & " = '" & EncryptVal & "'")
                Else
                    qS.AppendString("Update " & cmbTable.Text & " Set " & cmbColumn.Text & " = '" & TxtInsert.Text & "'")
                End If

                If cmbWhereCol.Text <> "" And cmbOperator.Text <> "" Then
                    qS.AppendString(" Where " & cmbWhereCol.Text & " " & OpWhere)
                End If
            End If
        ElseIf rbReplace.Checked Then
            If cmbTable.Text = "" Or cmbColumn.Text = "" Or TxtFind.Text = "" Or TxtInsert.Text = "" Then
                MessageBox.Show("Please fill out all values boxes before viewing your final query.", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Return
            Else
                Dim OpWhere
                Select Case cmbOperator.Text.ToLower
                    Case "begins with"
                        OpWhere = " LIKE '" & cmbWhereVal.Text & "%'"
                    Case "ends with"
                        OpWhere = " LIKE '%" & cmbWhereVal.Text & "'"
                    Case "contains"
                        OpWhere = " LIKE '%" & cmbWhereVal.Text & "%'"
                    Case "does not contain"
                        OpWhere = " NOT LIKE '%" & cmbWhereVal.Text & "%'"
                    Case "does not begin with"
                        OpWhere = " NOT LIKE '" & cmbWhereVal.Text & "%'"
                    Case "does not end with"
                        OpWhere = " NOT LIKE '%" & cmbWhereVal.Text & "'"
                    Case "is empty"
                        OpWhere = " IS NULL OR " &
                        cmbTable.Text & "." & cmbWhereCol.Text & " ='')"
                    Case "is not empty"
                        OpWhere = " IS NOT NULL AND " &
                        cmbTable.Text & "." & cmbWhereCol.Text & " <> '')"
                    Case Else
                        OpWhere = cmbOperator.Text & " '" & cmbWhereVal.Text & "'"
                End Select
                If dtype.ToLower = "text" Then
                    qS.AppendString("Update " & cmbTable.Text & " Set " & cmbColumn.Text & " = Replace(CAST(" & cmbColumn.Text & " AS VARCHAR(MAX)), '" & TxtFind.Text & "', '" & TxtInsert.Text & "')")
                Else
                    qS.AppendString("Update " & cmbTable.Text & " Set " & cmbColumn.Text & " = Replace(" & cmbColumn.Text & ", '" & TxtFind.Text & "', '" & TxtInsert.Text & "')")
                End If
                If cmbWhereCol.Text <> "" And cmbOperator.Text <> "" Then
                    qS.AppendString(" Where " & cmbWhereCol.Text & " " & OpWhere)
                End If
            End If
        End If
        pnlTableColumn.Visible = False
        QueryPanel.Visible = True
        fctbQuery.Text = qS.ReturnString()
        qS.ClearString()
    End Sub

    Private Sub btnBack_Click(sender As Object, e As EventArgs) Handles btnBack.Click
		QueryPanel.Visible = False
		pnlTableColumn.Visible = True
		btnExecute.Enabled = False
	End Sub

	Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click
		Dim con As SqlConnection = New SqlConnection(ConString)
		con.Open()
		Dim NoExec As String = "SET NOEXEC ON; " & fctbQuery.Text & "; SET NOEXEC OFF;"
		Dim cmd As SqlCommand = New SqlCommand(NoExec, con)
		Try
            Dim numofRows = cmd.ExecuteNonQuery()
            MessageBox.Show("Query returned valid! Ready for Execution!", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Information)
            btnExecute.Enabled = True
		Catch ex As Exception
			MessageBox.Show("Query returned invalid, please check syntax and parameters!", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
		End Try

	End Sub

	Private Sub btnExecute_Click(sender As Object, e As EventArgs) Handles btnExecute.Click
		Dim con As SqlConnection = New SqlConnection(ConString)
		con.Open()
		Dim cmd As SqlCommand = New SqlCommand(fctbQuery.Text, con)
		Try
            Dim numofRows = cmd.ExecuteNonQuery()
            MessageBox.Show("Query Execution Complete, the table has been updated! Number of Rows that may have been affected: " & numofRows, "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Information)
            fctbQuery.Clear()
			btnExecute.Enabled = False
			QueryPanel.Visible = False
			pnlTableColumn.Visible = True
		Catch ex As Exception
			MessageBox.Show("Query Execution Failed!", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
		End Try

	End Sub

    Private Sub rbOverwrite_MouseHover(sender As Object, e As EventArgs) Handles rbOverwrite.MouseHover
        txtInfo.Visible = True
        txtInfo.Text = "Overwrite will replace an entire value in a data table's cell (or cells)"
    End Sub

    Private Sub rbReplace_MouseHover(sender As Object, e As EventArgs) Handles rbReplace.MouseHover
        txtInfo.Visible = True
        txtInfo.Text = "Replace changes part of a value in a data table's cell (or cells)"
    End Sub

    Private Sub rbReplace_MouseLeave(sender As Object, e As EventArgs) Handles rbReplace.MouseLeave, rbOverwrite.MouseLeave
        txtInfo.Visible = False
    End Sub
End Class