﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmConnect
	Inherits System.Windows.Forms.Form

	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()>
	Protected Overrides Sub Dispose(ByVal disposing As Boolean)
		Try
			If disposing AndAlso components IsNot Nothing Then
				components.Dispose()
			End If
		Finally
			MyBase.Dispose(disposing)
		End Try
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()>
	Private Sub InitializeComponent()
		Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmConnect))
		Me.cmbProduct = New System.Windows.Forms.ComboBox()
		Me.btnConnect = New System.Windows.Forms.Button()
		Me.Label1 = New System.Windows.Forms.Label()
		Me.btnFolderChooser = New System.Windows.Forms.Button()
		Me.txtBackupPath = New System.Windows.Forms.TextBox()
		Me.pbExp = New System.Windows.Forms.PictureBox()
		Me.btnABORT = New System.Windows.Forms.Button()
		Me.pnlMainStuff = New System.Windows.Forms.Panel()
		CType(Me.pbExp, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.pnlMainStuff.SuspendLayout()
		Me.SuspendLayout()
		'
		'cmbProduct
		'
		Me.cmbProduct.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.cmbProduct.FormattingEnabled = True
		Me.cmbProduct.Items.AddRange(New Object() {"CRD", "MARS", "PBRS", "SQLRD"})
		Me.cmbProduct.Location = New System.Drawing.Point(30, 38)
		Me.cmbProduct.Name = "cmbProduct"
		Me.cmbProduct.Size = New System.Drawing.Size(262, 21)
		Me.cmbProduct.TabIndex = 0
		'
		'btnConnect
		'
		Me.btnConnect.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.btnConnect.BackColor = System.Drawing.SystemColors.ControlLight
		Me.btnConnect.Cursor = System.Windows.Forms.Cursors.Default
		Me.btnConnect.FlatAppearance.BorderColor = System.Drawing.Color.Black
		Me.btnConnect.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
		Me.btnConnect.ForeColor = System.Drawing.Color.Black
		Me.btnConnect.Location = New System.Drawing.Point(30, 91)
		Me.btnConnect.Name = "btnConnect"
		Me.btnConnect.Size = New System.Drawing.Size(126, 24)
		Me.btnConnect.TabIndex = 4
		Me.btnConnect.Text = "Connect"
		Me.btnConnect.UseVisualStyleBackColor = False
		'
		'Label1
		'
		Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.Label1.AutoSize = True
		Me.Label1.BackColor = System.Drawing.Color.Transparent
		Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.ForeColor = System.Drawing.Color.Black
		Me.Label1.Location = New System.Drawing.Point(29, 18)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(104, 17)
		Me.Label1.TabIndex = 3
		Me.Label1.Text = "Select Product:"
		'
		'btnFolderChooser
		'
		Me.btnFolderChooser.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.btnFolderChooser.BackColor = System.Drawing.SystemColors.ControlLight
		Me.btnFolderChooser.FlatAppearance.BorderColor = System.Drawing.Color.Black
		Me.btnFolderChooser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red
		Me.btnFolderChooser.ForeColor = System.Drawing.Color.Black
		Me.btnFolderChooser.Location = New System.Drawing.Point(263, 65)
		Me.btnFolderChooser.Name = "btnFolderChooser"
		Me.btnFolderChooser.Size = New System.Drawing.Size(29, 20)
		Me.btnFolderChooser.TabIndex = 3
		Me.btnFolderChooser.Text = "..."
		Me.btnFolderChooser.TextAlign = System.Drawing.ContentAlignment.TopCenter
		Me.btnFolderChooser.UseVisualStyleBackColor = False
		'
		'txtBackupPath
		'
		Me.txtBackupPath.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.txtBackupPath.Location = New System.Drawing.Point(30, 65)
		Me.txtBackupPath.Name = "txtBackupPath"
		Me.txtBackupPath.Size = New System.Drawing.Size(227, 20)
		Me.txtBackupPath.TabIndex = 2
		Me.txtBackupPath.Text = "Select a backup destination..."
		'
		'pbExp
		'
		Me.pbExp.BackColor = System.Drawing.Color.Transparent
		Me.pbExp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
		Me.pbExp.Enabled = False
		Me.pbExp.InitialImage = Nothing
		Me.pbExp.Location = New System.Drawing.Point(41, 3)
		Me.pbExp.Name = "pbExp"
		Me.pbExp.Size = New System.Drawing.Size(135, 150)
		Me.pbExp.TabIndex = 6
		Me.pbExp.TabStop = False
		Me.pbExp.Visible = False
		'
		'btnABORT
		'
		Me.btnABORT.Anchor = System.Windows.Forms.AnchorStyles.None
		Me.btnABORT.BackColor = System.Drawing.SystemColors.ControlLight
		Me.btnABORT.DialogResult = System.Windows.Forms.DialogResult.Cancel
		Me.btnABORT.FlatAppearance.BorderColor = System.Drawing.Color.Black
		Me.btnABORT.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
		Me.btnABORT.ForeColor = System.Drawing.Color.Black
		Me.btnABORT.Location = New System.Drawing.Point(171, 91)
		Me.btnABORT.Name = "btnABORT"
		Me.btnABORT.Size = New System.Drawing.Size(121, 24)
		Me.btnABORT.TabIndex = 5
		Me.btnABORT.Text = "Cancel"
		Me.btnABORT.UseVisualStyleBackColor = False
		'
		'pnlMainStuff
		'
		Me.pnlMainStuff.BackColor = System.Drawing.Color.Silver
		Me.pnlMainStuff.Controls.Add(Me.txtBackupPath)
		Me.pnlMainStuff.Controls.Add(Me.btnFolderChooser)
		Me.pnlMainStuff.Controls.Add(Me.Label1)
		Me.pnlMainStuff.Controls.Add(Me.btnABORT)
		Me.pnlMainStuff.Controls.Add(Me.btnConnect)
		Me.pnlMainStuff.Controls.Add(Me.cmbProduct)
		Me.pnlMainStuff.Location = New System.Drawing.Point(12, 12)
		Me.pnlMainStuff.Name = "pnlMainStuff"
		Me.pnlMainStuff.Size = New System.Drawing.Size(323, 130)
		Me.pnlMainStuff.TabIndex = 7
		'
		'frmConnect
		'
		Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.Color.Silver
		Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
		Me.CancelButton = Me.btnABORT
		Me.ClientSize = New System.Drawing.Size(347, 154)
		Me.Controls.Add(Me.pnlMainStuff)
		Me.Controls.Add(Me.pbExp)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.Name = "frmConnect"
		Me.Text = "Bulk Updater"
		CType(Me.pbExp, System.ComponentModel.ISupportInitialize).EndInit()
		Me.pnlMainStuff.ResumeLayout(False)
		Me.pnlMainStuff.PerformLayout()
		Me.ResumeLayout(False)

	End Sub

	Friend WithEvents cmbProduct As ComboBox
	Friend WithEvents btnConnect As Button
	Friend WithEvents Label1 As Label
	Friend WithEvents btnFolderChooser As Button
	Friend WithEvents txtBackupPath As TextBox
	Friend WithEvents pbExp As PictureBox
	Friend WithEvents btnABORT As Button
	Friend WithEvents pnlMainStuff As Panel
End Class
