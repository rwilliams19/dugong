﻿Imports System.Data.Odbc
Imports System.Data.SqlClient
Imports System.IO.Compression

Public Class frmConnect
	Public ConString As String
	Public ConType As String
	Public CustNo As String
    Public prod As String
#If DEBUG Then
    Dim sAppPath As String = "C:\Program Files (x86)\ChristianSteven"
#Else
	Dim sAppPath As String = IO.Path.GetDirectoryName(Application.StartupPath)
#End If

	Private Sub btnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click

		If cmbProduct.Text = "" Then
			MessageBox.Show("Please select a product.", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
		ElseIf IO.Directory.Exists(txtBackupPath.Text) = False Then
			MessageBox.Show("Please select a valid folder path.", "Bulk Update", MessageBoxButtons.OK, MessageBoxIcon.Warning)
		Else

			Cursor = Cursors.AppStarting
			If Date.Now.Month = 4 And Date.Now.Day = 1 Then
				pnlMainStuff.Visible = False
				pbExp.Enabled = True
				pbExp.Visible = True
			End If
			prod = cmbProduct.Text.ToLower
			Dim settingMan As New clsSettingsManager
			Dim ConfigPath As String

			ConfigPath = System.IO.Path.Combine(sAppPath, cmbProduct.Text, cmbProduct.Text.ToLower & "live.config")
			' Uses clsSettingManager to get the con string form the live.config file
			ConType = settingMan.GetSettingValue("ConType", ConfigPath, False)
			ConString = settingMan.GetSettingValue("ConString", ConfigPath, True)
			CustNo = settingMan.GetSettingValue("custno", ConfigPath, False)
			ConString = localConStringdotNET()

			GenerateBackup()
			Cursor = Cursors.Default
			'Loads form for selecting column, tble, etc.
			Dim SL As frmSelectCol = New frmSelectCol
			SL.Show()
			Me.Close()

		End If
	End Sub

	Private Sub btnFolderChooser_Click(sender As Object, e As EventArgs) Handles btnFolderChooser.Click
		Dim fbd As FolderBrowserDialog = New FolderBrowserDialog
		If fbd.ShowDialog() = DialogResult.OK Then
			txtBackupPath.Text = fbd.SelectedPath
		End If
	End Sub

	Private Sub btnABORT_Click(sender As Object, e As EventArgs) Handles btnABORT.Click
		Close()
	End Sub

	Private Function localConStringdotNET() As String
		Try
			'process constring
			Dim dataSource, catalog, userid, password, persist, integratedsecurity As String

			'For non-standard
			'using Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=SQL-RD;Data Source=FDCA01
			'for non standard using NT auth Data Source=AUTOSERVER1;Initial Catalog=crd;Integrated Security=True
			If ConString <> "" Then
				For Each s As String In ConString.Split(";")
					Dim key, value As String

					key = s.Split("=")(0)
					value = s.Split("=")(1)

					Select Case key.ToLower
						Case "initial catalog"
							catalog = value
						Case "data source"
							dataSource = value
						Case "password"
							password = value
						Case "user id"
							userid = value
						Case "persist security info"
							persist = value
						Case "integrated security"
							integratedsecurity = value
					End Select
				Next

				Dim newCon As String = "Data Source=" & dataSource & ";Initial Catalog=" & catalog

				If persist <> "" Then newCon &= ";Persist Security Info=" & persist
				If userid <> "" Then newCon &= ";User ID=" & userid
				If password <> "" Then newCon &= ";Password=" & password
				If integratedsecurity <> "" Then newCon &= ";Integrated Security=" & integratedsecurity

				Return newCon
			Else
				Return "Data Source=" & Environment.MachineName & "\CRD;Initial Catalog=CRD;Persist Security Info=True;User ID=sa;Password=cssAdmin12345$"
			End If
		Catch
			Return "Data Source=" & Environment.MachineName & "\CRD;Initial Catalog=CRD;Persist Security Info=True;User ID=sa;Password=cssAdmin12345$"
		End Try
	End Function

	Private Sub GenerateBackup()
		'Generates a backup of the DB before any changes can be made
		Dim srcDSN As String
		Dim dsn As String
		Dim user As String
		Dim pwd As String
		Dim ok As Boolean
		If ConType = "LOCAL" Then

			Dim dm As New clsDataMigrator(clsDataMigrator.dbType.LOCAL)
			Dim errInfo As Exception = Nothing

			ok = dm.Export("", sAppPath & "\" & prod.ToUpper & "\" & prod & "live.def", errInfo)

			If ok = False Then
				If errInfo Is Nothing Then
					Throw New Exception("Could not create def file")
				Else
					Throw errInfo
				End If
			End If

		ElseIf ConType = "ODBC" Then

			If ConString.Length > 0 Then
				dsn = ConString.Split(";")(4).Split("=")(1)
				user = ConString.Split(";")(3).Split("=")(1)
				pwd = ConString.Split(";")(1).Split("=")(1)
				srcDSN = "DSN=" & dsn & ";Uid=" & user & ";Pwd=" & pwd & ";"
			End If

			Dim dm As New clsDataMigrator(clsDataMigrator.dbType.ODBC)
			Dim errInfo As Exception = Nothing

			ok = dm.Export(ConString, sAppPath & "\" & prod.ToUpper & "\" & prod & "live.def", errInfo)

			If ok = False Then
				If errInfo Is Nothing Then
					Throw New Exception("Could not create def file")
				Else
					Throw errInfo
				End If
			End If

		Else
			ok = True
		End If

		Try
			Dim FilezToZip As String
			Dim ZipFile As String = prod.ToUpper & "_" & CustNo & "_" & Date.Now.ToString("yyyyMMdd")

			Try
				FilezToZip = sAppPath & "\" & prod.ToUpper & "\" & prod & "live.def"
			Catch ex As Exception
				MessageBox.Show("live.def file not found.")
			End Try

			ZipFiles(txtBackupPath.Text & "\" & ZipFile, FilezToZip)
			MessageBox.Show("Backup generated successfully." & Environment.NewLine & "Database connection established.", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Information)
		Catch ex As Exception
			End Try

	End Sub

	Public Sub ZipFiles(ByRef path As String, ByRef filesToZip As String)
		While IO.Directory.Exists(path)
			path &= 0
		End While
		IO.Directory.CreateDirectory(path)
		IO.File.Copy(filesToZip, path & "\" & prod & "live.def")
	End Sub

	Private Sub frmConnect_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		If Date.Now.Month = 4 And Now.Date.Day = 1 Then
			Me.BackgroundImage = Dugong.My.Resources.Resources.fire
		End If
	End Sub

	Private Sub cmbProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cmbProduct.SelectedIndexChanged
		Select Case cmbProduct.Text
			Case "CRD"
				Me.BackColor = Color.FromArgb(255, 76, 17)
			Case "SQLRD"
				Me.BackColor = Color.FromArgb(54, 171, 198)
			Case "MARS"
				Me.BackColor = Color.FromArgb(168, 32, 37)
			Case "PBRS"
				Me.BackColor = Color.FromArgb(37, 37, 38)
		End Select

	End Sub
End Class
