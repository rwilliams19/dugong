﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSelectCol
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSelectCol))
        Me.cmbTable = New System.Windows.Forms.ComboBox()
        Me.cmbColumn = New System.Windows.Forms.ComboBox()
        Me.LabelTable = New System.Windows.Forms.Label()
        Me.LabelColumn = New System.Windows.Forms.Label()
        Me.rbOverwrite = New System.Windows.Forms.RadioButton()
        Me.rbReplace = New System.Windows.Forms.RadioButton()
        Me.QueryPanel = New System.Windows.Forms.Panel()
        Me.fctbQuery = New FastColoredTextBoxNS.FastColoredTextBox()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.btnBack = New System.Windows.Forms.Button()
        Me.btnExecute = New System.Windows.Forms.Button()
        Me.LabelQuery = New System.Windows.Forms.Label()
        Me.LabelDescription = New System.Windows.Forms.Label()
        Me.pnlTableColumn = New System.Windows.Forms.Panel()
        Me.PictureBoxProd = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.chkEncrypt = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TxtInsert = New System.Windows.Forms.TextBox()
        Me.TxtFind = New System.Windows.Forms.TextBox()
        Me.LabelWhereCol = New System.Windows.Forms.Label()
        Me.cmbWhereVal = New System.Windows.Forms.ComboBox()
        Me.cmbOperator = New System.Windows.Forms.ComboBox()
        Me.btnVerify = New System.Windows.Forms.Button()
        Me.cmbWhereCol = New System.Windows.Forms.ComboBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImageList2 = New System.Windows.Forms.ImageList(Me.components)
        Me.ImgListProd = New System.Windows.Forms.ImageList(Me.components)
        Me.txtInfo = New System.Windows.Forms.RichTextBox()
        Me.QueryPanel.SuspendLayout()
        CType(Me.fctbQuery, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlTableColumn.SuspendLayout()
        CType(Me.PictureBoxProd, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmbTable
        '
        Me.cmbTable.FormattingEnabled = True
        Me.cmbTable.Location = New System.Drawing.Point(12, 26)
        Me.cmbTable.Name = "cmbTable"
        Me.cmbTable.Size = New System.Drawing.Size(200, 21)
        Me.cmbTable.Sorted = True
        Me.cmbTable.TabIndex = 0
        '
        'cmbColumn
        '
        Me.cmbColumn.FormattingEnabled = True
        Me.cmbColumn.Location = New System.Drawing.Point(12, 75)
        Me.cmbColumn.Name = "cmbColumn"
        Me.cmbColumn.Size = New System.Drawing.Size(200, 21)
        Me.cmbColumn.Sorted = True
        Me.cmbColumn.TabIndex = 1
        '
        'LabelTable
        '
        Me.LabelTable.AutoSize = True
        Me.LabelTable.Location = New System.Drawing.Point(9, 9)
        Me.LabelTable.Name = "LabelTable"
        Me.LabelTable.Size = New System.Drawing.Size(34, 13)
        Me.LabelTable.TabIndex = 2
        Me.LabelTable.Text = "Table"
        '
        'LabelColumn
        '
        Me.LabelColumn.AutoSize = True
        Me.LabelColumn.Location = New System.Drawing.Point(12, 59)
        Me.LabelColumn.Name = "LabelColumn"
        Me.LabelColumn.Size = New System.Drawing.Size(42, 13)
        Me.LabelColumn.TabIndex = 3
        Me.LabelColumn.Text = "Column"
        '
        'rbOverwrite
        '
        Me.rbOverwrite.AutoSize = True
        Me.rbOverwrite.Checked = True
        Me.rbOverwrite.Location = New System.Drawing.Point(12, 121)
        Me.rbOverwrite.Name = "rbOverwrite"
        Me.rbOverwrite.Size = New System.Drawing.Size(100, 17)
        Me.rbOverwrite.TabIndex = 2
        Me.rbOverwrite.TabStop = True
        Me.rbOverwrite.Text = "Overwrite Value"
        Me.rbOverwrite.UseVisualStyleBackColor = True
        '
        'rbReplace
        '
        Me.rbReplace.AutoSize = True
        Me.rbReplace.Location = New System.Drawing.Point(12, 144)
        Me.rbReplace.Name = "rbReplace"
        Me.rbReplace.Size = New System.Drawing.Size(129, 17)
        Me.rbReplace.TabIndex = 3
        Me.rbReplace.Text = "Replace Part of Value"
        Me.rbReplace.UseVisualStyleBackColor = True
        '
        'QueryPanel
        '
        Me.QueryPanel.Controls.Add(Me.fctbQuery)
        Me.QueryPanel.Controls.Add(Me.btnTest)
        Me.QueryPanel.Controls.Add(Me.btnBack)
        Me.QueryPanel.Controls.Add(Me.btnExecute)
        Me.QueryPanel.Controls.Add(Me.LabelQuery)
        Me.QueryPanel.Controls.Add(Me.LabelDescription)
        Me.QueryPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.QueryPanel.Location = New System.Drawing.Point(0, 0)
        Me.QueryPanel.Name = "QueryPanel"
        Me.QueryPanel.Size = New System.Drawing.Size(535, 367)
        Me.QueryPanel.TabIndex = 14
        Me.QueryPanel.Visible = False
        '
        'fctbQuery
        '
        Me.fctbQuery.AutoCompleteBracketsList = New Char() {Global.Microsoft.VisualBasic.ChrW(40), Global.Microsoft.VisualBasic.ChrW(41), Global.Microsoft.VisualBasic.ChrW(123), Global.Microsoft.VisualBasic.ChrW(125), Global.Microsoft.VisualBasic.ChrW(91), Global.Microsoft.VisualBasic.ChrW(93), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(34), Global.Microsoft.VisualBasic.ChrW(39), Global.Microsoft.VisualBasic.ChrW(39)}
        Me.fctbQuery.AutoIndentCharsPatterns = ""
        Me.fctbQuery.AutoScrollMinSize = New System.Drawing.Size(0, 14)
        Me.fctbQuery.BackBrush = Nothing
        Me.fctbQuery.CharHeight = 14
        Me.fctbQuery.CharWidth = 8
        Me.fctbQuery.CommentPrefix = "--"
        Me.fctbQuery.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.fctbQuery.DisabledColor = System.Drawing.Color.FromArgb(CType(CType(100, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer), CType(CType(180, Byte), Integer))
        Me.fctbQuery.Font = New System.Drawing.Font("Courier New", 9.75!)
        Me.fctbQuery.IsReplaceMode = False
        Me.fctbQuery.Language = FastColoredTextBoxNS.Language.SQL
        Me.fctbQuery.LeftBracket = Global.Microsoft.VisualBasic.ChrW(40)
        Me.fctbQuery.Location = New System.Drawing.Point(12, 99)
        Me.fctbQuery.Name = "fctbQuery"
        Me.fctbQuery.Paddings = New System.Windows.Forms.Padding(0)
        Me.fctbQuery.RightBracket = Global.Microsoft.VisualBasic.ChrW(41)
        Me.fctbQuery.SelectionColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.fctbQuery.ServiceColors = CType(resources.GetObject("fctbQuery.ServiceColors"), FastColoredTextBoxNS.ServiceColors)
        Me.fctbQuery.ShowLineNumbers = False
        Me.fctbQuery.Size = New System.Drawing.Size(511, 200)
        Me.fctbQuery.TabIndex = 15
        Me.fctbQuery.WordWrap = True
        Me.fctbQuery.Zoom = 100
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(448, 305)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(75, 23)
        Me.btnTest.TabIndex = 12
        Me.btnTest.Text = "Test"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'btnBack
        '
        Me.btnBack.Location = New System.Drawing.Point(12, 336)
        Me.btnBack.Name = "btnBack"
        Me.btnBack.Size = New System.Drawing.Size(75, 23)
        Me.btnBack.TabIndex = 13
        Me.btnBack.Text = "Back"
        Me.btnBack.UseVisualStyleBackColor = True
        '
        'btnExecute
        '
        Me.btnExecute.Enabled = False
        Me.btnExecute.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExecute.Location = New System.Drawing.Point(373, 331)
        Me.btnExecute.Name = "btnExecute"
        Me.btnExecute.Size = New System.Drawing.Size(150, 28)
        Me.btnExecute.TabIndex = 14
        Me.btnExecute.Text = "Confirm and Execute"
        Me.btnExecute.UseVisualStyleBackColor = True
        '
        'LabelQuery
        '
        Me.LabelQuery.AutoSize = True
        Me.LabelQuery.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelQuery.Location = New System.Drawing.Point(14, 76)
        Me.LabelQuery.Name = "LabelQuery"
        Me.LabelQuery.Size = New System.Drawing.Size(165, 16)
        Me.LabelQuery.TabIndex = 6
        Me.LabelQuery.Text = "SQL Query For Execution"
        '
        'LabelDescription
        '
        Me.LabelDescription.AutoSize = True
        Me.LabelDescription.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.LabelDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LabelDescription.Font = New System.Drawing.Font("Arial Rounded MT Bold", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelDescription.Location = New System.Drawing.Point(79, 9)
        Me.LabelDescription.Name = "LabelDescription"
        Me.LabelDescription.Size = New System.Drawing.Size(383, 58)
        Me.LabelDescription.TabIndex = 5
        Me.LabelDescription.Text = resources.GetString("LabelDescription.Text")
        Me.LabelDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pnlTableColumn
        '
        Me.pnlTableColumn.BackColor = System.Drawing.Color.Silver
        Me.pnlTableColumn.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnlTableColumn.Controls.Add(Me.txtInfo)
        Me.pnlTableColumn.Controls.Add(Me.PictureBoxProd)
        Me.pnlTableColumn.Controls.Add(Me.PictureBox2)
        Me.pnlTableColumn.Controls.Add(Me.chkEncrypt)
        Me.pnlTableColumn.Controls.Add(Me.Label2)
        Me.pnlTableColumn.Controls.Add(Me.Label1)
        Me.pnlTableColumn.Controls.Add(Me.TxtInsert)
        Me.pnlTableColumn.Controls.Add(Me.TxtFind)
        Me.pnlTableColumn.Controls.Add(Me.LabelWhereCol)
        Me.pnlTableColumn.Controls.Add(Me.cmbWhereVal)
        Me.pnlTableColumn.Controls.Add(Me.cmbOperator)
        Me.pnlTableColumn.Controls.Add(Me.btnVerify)
        Me.pnlTableColumn.Controls.Add(Me.cmbWhereCol)
        Me.pnlTableColumn.Controls.Add(Me.cmbTable)
        Me.pnlTableColumn.Controls.Add(Me.LabelTable)
        Me.pnlTableColumn.Controls.Add(Me.cmbColumn)
        Me.pnlTableColumn.Controls.Add(Me.LabelColumn)
        Me.pnlTableColumn.Controls.Add(Me.rbOverwrite)
        Me.pnlTableColumn.Controls.Add(Me.rbReplace)
        Me.pnlTableColumn.Controls.Add(Me.PictureBox1)
        Me.pnlTableColumn.Location = New System.Drawing.Point(0, 0)
        Me.pnlTableColumn.Name = "pnlTableColumn"
        Me.pnlTableColumn.Size = New System.Drawing.Size(534, 367)
        Me.pnlTableColumn.TabIndex = 15
        '
        'PictureBoxProd
        '
        Me.PictureBoxProd.Location = New System.Drawing.Point(290, 69)
        Me.PictureBoxProd.MaximumSize = New System.Drawing.Size(250, 120)
        Me.PictureBoxProd.MinimumSize = New System.Drawing.Size(150, 110)
        Me.PictureBoxProd.Name = "PictureBoxProd"
        Me.PictureBoxProd.Size = New System.Drawing.Size(218, 110)
        Me.PictureBoxProd.TabIndex = 25
        Me.PictureBoxProd.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = Global.Dugong.My.Resources.Resources.logo_compressor__1_
        Me.PictureBox2.Location = New System.Drawing.Point(290, 26)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(218, 39)
        Me.PictureBox2.TabIndex = 24
        Me.PictureBox2.TabStop = False
        '
        'chkEncrypt
        '
        Me.chkEncrypt.AutoSize = True
        Me.chkEncrypt.Location = New System.Drawing.Point(12, 248)
        Me.chkEncrypt.Name = "chkEncrypt"
        Me.chkEncrypt.Size = New System.Drawing.Size(92, 17)
        Me.chkEncrypt.TabIndex = 17
        Me.chkEncrypt.Text = "Encrypt Value"
        Me.chkEncrypt.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(182, 206)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 13)
        Me.Label2.TabIndex = 23
        Me.Label2.Text = "Value to Repalce"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 206)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Value to Insert"
        '
        'TxtInsert
        '
        Me.TxtInsert.Location = New System.Drawing.Point(12, 222)
        Me.TxtInsert.Name = "TxtInsert"
        Me.TxtInsert.Size = New System.Drawing.Size(167, 20)
        Me.TxtInsert.TabIndex = 15
        '
        'TxtFind
        '
        Me.TxtFind.Location = New System.Drawing.Point(185, 222)
        Me.TxtFind.Name = "TxtFind"
        Me.TxtFind.Size = New System.Drawing.Size(167, 20)
        Me.TxtFind.TabIndex = 16
        '
        'LabelWhereCol
        '
        Me.LabelWhereCol.AutoSize = True
        Me.LabelWhereCol.Location = New System.Drawing.Point(9, 281)
        Me.LabelWhereCol.Name = "LabelWhereCol"
        Me.LabelWhereCol.Size = New System.Drawing.Size(110, 13)
        Me.LabelWhereCol.TabIndex = 14
        Me.LabelWhereCol.Text = "Only Results Where..."
        '
        'cmbWhereVal
        '
        Me.cmbWhereVal.FormattingEnabled = True
        Me.cmbWhereVal.Location = New System.Drawing.Point(358, 297)
        Me.cmbWhereVal.Name = "cmbWhereVal"
        Me.cmbWhereVal.Size = New System.Drawing.Size(167, 21)
        Me.cmbWhereVal.Sorted = True
        Me.cmbWhereVal.TabIndex = 20
        '
        'cmbOperator
        '
        Me.cmbOperator.FormattingEnabled = True
        Me.cmbOperator.Items.AddRange(New Object() {"BEGINS WITH", "ENDS WITH", "CONTAINS", "DOES NOT CONTAIN", "DOES NOT BEGIN WITH", "DOES NOT END WITH", "IS EMPTY", "IS NOT EMPTY", "<", "<=", "<>", "=", ">=", ">"})
        Me.cmbOperator.Location = New System.Drawing.Point(185, 297)
        Me.cmbOperator.Name = "cmbOperator"
        Me.cmbOperator.Size = New System.Drawing.Size(167, 21)
        Me.cmbOperator.TabIndex = 19
        '
        'btnVerify
        '
        Me.btnVerify.Image = CType(resources.GetObject("btnVerify.Image"), System.Drawing.Image)
        Me.btnVerify.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnVerify.Location = New System.Drawing.Point(448, 331)
        Me.btnVerify.Name = "btnVerify"
        Me.btnVerify.Size = New System.Drawing.Size(75, 23)
        Me.btnVerify.TabIndex = 21
        Me.btnVerify.Text = "Verify"
        Me.btnVerify.UseVisualStyleBackColor = True
        '
        'cmbWhereCol
        '
        Me.cmbWhereCol.FormattingEnabled = True
        Me.cmbWhereCol.Location = New System.Drawing.Point(12, 297)
        Me.cmbWhereCol.Name = "cmbWhereCol"
        Me.cmbWhereCol.Size = New System.Drawing.Size(167, 21)
        Me.cmbWhereCol.Sorted = True
        Me.cmbWhereCol.TabIndex = 18
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PictureBox1.Enabled = False
        Me.PictureBox1.Image = Global.Dugong.My.Resources.Resources.size_restricted_dugong
        Me.PictureBox1.Location = New System.Drawing.Point(243, 9)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(280, 170)
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        Me.PictureBox1.Visible = False
        '
        'ImageList1
        '
        Me.ImageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList1.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        '
        'ImageList2
        '
        Me.ImageList2.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me.ImageList2.ImageSize = New System.Drawing.Size(16, 16)
        Me.ImageList2.TransparentColor = System.Drawing.Color.Transparent
        '
        'ImgListProd
        '
        Me.ImgListProd.ImageStream = CType(resources.GetObject("ImgListProd.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImgListProd.TransparentColor = System.Drawing.Color.Transparent
        Me.ImgListProd.Images.SetKeyName(0, "CRD_Logo_2016 (1).jpg")
        Me.ImgListProd.Images.SetKeyName(1, "MARS-logo-left (1).png")
        Me.ImgListProd.Images.SetKeyName(2, "PBRS-logo-2018-RGB (1).png")
        Me.ImgListProd.Images.SetKeyName(3, "SQL-logo-2016-RGB-HR (1).png")
        '
        'txtInfo
        '
        Me.txtInfo.Location = New System.Drawing.Point(147, 102)
        Me.txtInfo.Name = "txtInfo"
        Me.txtInfo.Size = New System.Drawing.Size(124, 77)
        Me.txtInfo.TabIndex = 27
        Me.txtInfo.Text = ""
        Me.txtInfo.Visible = False
        '
        'frmSelectCol
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(535, 367)
        Me.Controls.Add(Me.pnlTableColumn)
        Me.Controls.Add(Me.QueryPanel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSelectCol"
        Me.Text = "Select Column/Value To Change"
        Me.QueryPanel.ResumeLayout(False)
        Me.QueryPanel.PerformLayout()
        CType(Me.fctbQuery, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlTableColumn.ResumeLayout(False)
        Me.pnlTableColumn.PerformLayout()
        CType(Me.PictureBoxProd, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents cmbTable As ComboBox
    Friend WithEvents cmbColumn As ComboBox
    Friend WithEvents LabelTable As Label
    Friend WithEvents LabelColumn As Label
	Friend WithEvents PictureBox1 As PictureBox
	Friend WithEvents rbOverwrite As RadioButton
	Friend WithEvents rbReplace As RadioButton
	Friend WithEvents QueryPanel As Panel
	Friend WithEvents LabelDescription As Label
	Friend WithEvents LabelQuery As Label
	Friend WithEvents btnTest As Button
	Friend WithEvents btnBack As Button
	Friend WithEvents btnExecute As Button
	Friend WithEvents pnlTableColumn As Panel
	Friend WithEvents chkEncrypt As CheckBox
	Friend WithEvents Label2 As Label
	Friend WithEvents Label1 As Label
	Friend WithEvents TxtInsert As TextBox
	Friend WithEvents TxtFind As TextBox
	Friend WithEvents LabelWhereCol As Label
	Friend WithEvents cmbWhereVal As ComboBox
	Friend WithEvents cmbOperator As ComboBox
	Friend WithEvents btnVerify As Button
	Friend WithEvents cmbWhereCol As ComboBox
	Friend WithEvents fctbQuery As FastColoredTextBoxNS.FastColoredTextBox
	Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents ImageList1 As ImageList
    Friend WithEvents ImageList2 As ImageList
    Friend WithEvents ImgListProd As ImageList
    Friend WithEvents PictureBoxProd As PictureBox
    Friend WithEvents txtInfo As RichTextBox
End Class
