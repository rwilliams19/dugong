﻿Imports System.Data.Odbc
Imports System.Data.SqlClient

Public Class clsDataMigrator
	Sub New(ByVal databaseType As dbType)
		Me.m_databaseType = databaseType
	End Sub
	Enum dbType
		ODBC = 0
		LOCAL = 1
	End Enum

	Dim ConType As dbType
	Dim m_tablesToInclude As DataTable = Nothing
	Public Property m_databaseType() As dbType
		Get
			Return ConType
		End Get
		Set(ByVal value As dbType)
			ConType = value
		End Set
	End Property

	Public Property tablesToInclude As DataTable
		Get
			Return m_tablesToInclude
		End Get
		Set(value As DataTable)
			m_tablesToInclude = value
		End Set
	End Property

	Public Overloads Function Export(ByVal srcDSN As String) As DataSet
		Try
			Dim ds As DataSet = New DataSet
			Dim err As Exception

			If Me.m_databaseType = dbType.ODBC Then
				ExportToDataSet(srcDSN, ds, err)
			ElseIf m_databaseType = dbType.LOCAL Then
				ExportToDataSet(ds, err)
			End If

			Return ds
		Catch ex As Exception
			Return Nothing
		End Try
	End Function

	Public Overloads Function Export(ByVal srcDSN As String, ByVal fileName As String, ByRef errInfo As Exception) As Boolean
		Dim rc As Boolean = False
		Try
			Dim ds As DataSet = New DataSet

			If Me.m_databaseType = dbType.ODBC Then
				ExportToDataSet(srcDSN, ds, errInfo)
			Else
				ExportToDataSet(ds, errInfo)
			End If

			'Write the dataset (including the table schema) as XML to the supplied file
			ds.WriteXml(fileName, XmlWriteMode.WriteSchema)

			rc = True
		Catch
		End Try

		Return rc
	End Function

	'Export to data set for ODBC connection
	Private Overloads Sub ExportToDataSet(ByVal srcDSN As String, ByRef ds As DataSet, ByRef errInfo As Exception)
		'Create the source ODBC connection object
		Dim odbcCon As OdbcConnection = New OdbcConnection(srcDSN)

		Try
			'Open a connection to the ODBC source
			odbcCon.Open()

			'Retrieve the Table schema info
			Dim tbls As DataTable = odbcCon.GetSchema("Tables")

			'For each user table, retrieve all the data and insert into our dataset
			For Each dr As DataRow In tbls.Rows

				Dim tableName As String = dr("TABLE_NAME")
				Dim tableType As String = dr("TABLE_TYPE")

				'Only get the data for the user tables (non-system)
				If (tableType = "TABLE") Then

					Dim ad As OdbcDataAdapter = New OdbcDataAdapter("SELECT * FROM [" + tableName + "]", odbcCon)

					Try
						'Add any schema info to the dataset first. This allows for proper key creation later
						ad.FillSchema(ds, SchemaType.Mapped, tableName)

						'Fill the dataset with the rows from the table
						ad.Fill(ds, tableName)

					Catch ex As OdbcException
						Debug.Print(ex.Message)
					Catch ex As Exception
						Debug.Print(ex.Message)
					End Try

				End If

			Next
			odbcCon.Close()

		Catch ex As OdbcException
			Debug.Print(ex.Message)

		Catch ex As Exception
			Debug.Print(ex.Message)

		Finally
			'Manually dispose of the odbc connection resource
			odbcCon.Dispose()
			odbcCon = Nothing
		End Try
	End Sub

	'Export to data set for LOCAL (SQL) connection
	Private Overloads Sub ExportToDataSet(ByRef ds As DataSet, ByRef errInfo As Exception)
		'Create the source LOCAL (SQL) connection object
		Dim sqlCon As SqlConnection = New SqlConnection(frmConnect.ConString)

		Try
			'Open a connection to the LOCAL (SQL) source
			sqlCon.Open()
			'Retrieve the Table schema info
			Dim tbls As DataTable

			If tablesToInclude Is Nothing Then
				tbls = sqlCon.GetSchema("Tables")
			Else
				tbls = tablesToInclude
			End If

			'For each user table, retrieve all the data and insert into our dataset
			For Each dr As DataRow In tbls.Rows

				Dim tableName As String = dr("TABLE_NAME")
				Dim tableType As String = dr("TABLE_TYPE")


				'Only get the data for the user tables (non-system)
				If (tableType = "TABLE") Or (tableType = "BASE TABLE") Then

					Dim ad As SqlDataAdapter = New SqlDataAdapter("SELECT * FROM [" + tableName + "]", sqlCon)

					Try
						'Add any schema info to the dataset first. This allows for proper key creation later
						ad.FillSchema(ds, SchemaType.Mapped, tableName)

						'Fill the dataset with the rows from the table
						ad.Fill(ds, tableName)
					Catch : End Try
				End If
			Next

			sqlCon.Close()

		Catch ex As OdbcException
			errInfo = ex

		Catch ex As Exception
			errInfo = ex

		Finally
			'Manually dispose of the LOCAL (SQL) connection resource
			sqlCon.Dispose()
			sqlCon = Nothing
		End Try
	End Sub
End Class
