﻿Public Class QueryBuilder
    Public Shared qString As String = ""
    Public Shared completed As Boolean

    Public Sub AppendString(ByVal aString As String)
        qString = qString & aString
    End Sub

    Public Sub ClearString()
        qString = ""
    End Sub

    Public Function ReturnString() As String
        Return qString
    End Function


End Class
