Imports System
Imports System.Xml
Imports System.IO
Imports System.Security.Cryptography

Public Class clsSettingsManager
	Dim mProvider As TripleDESCryptoServiceProvider
	Public Shared Appconfig As New clsSettingsManager
	Public Shared m_configTable As Hashtable

	Sub New()
		mProvider = New TripleDESCryptoServiceProvider
		mProvider.Key = New Byte() {112, 223, 122, 83, 173, 22, 186, 153, 229, 183, 73, 133, 124, 124, 132, 13}
		mProvider.IV = New Byte() {173, 224, 14, 43, 253, 103, 82, 212}
	End Sub

	Public Overloads Function GetSettingValue(ByVal SettingName As String, ByVal ConfigFile As String, Optional ByVal Decrypt As Boolean = False) As Object

		Dim oDoc As XmlDocument = New Xml.XmlDocument
		Dim oNodes As XmlNodeList
		Dim oNode As XmlNode
		Dim ConString As String = ""

		oDoc.Load(ConfigFile)

		oNodes = oDoc.GetElementsByTagName("setting")

		For Each oNode In oNodes
			Dim Attr As String = ""

			Attr = oNode.Attributes("name").Value

			If Attr.ToLower = SettingName.ToLower Then
				ConString = oNode.ChildNodes(0).InnerText

				If Decrypt = True Then
					ConString = DecryptString(ConString)
				End If

				Exit For
			End If
		Next
		Return ConString
	End Function

	Public Function EncryptString(ByVal AString As String) As String
		If AString = String.Empty Then
			Return AString
		Else
			Dim encryptedData() As Byte
			Dim dataStream As MemoryStream

			Dim encryptor As ICryptoTransform
			encryptor = mProvider.CreateEncryptor()

			Try
				dataStream = New MemoryStream

				Dim encryptedStream As CryptoStream
				Try
					'Create the encrypted stream
					encryptedStream = New CryptoStream(dataStream, encryptor, CryptoStreamMode.Write)

					Dim theWriter As StreamWriter
					Try
						'Write the string to memory via the encryption algorithm
						theWriter = New StreamWriter(encryptedStream)
						'Write the string to the memory stream
						theWriter.Write(AString)

						'End the writing
						theWriter.Flush()
						encryptedStream.FlushFinalBlock()

						'Position back at start
						dataStream.Position = 0

						'Create area for data
						ReDim encryptedData(CInt(dataStream.Length))

						'Read data from memory
						dataStream.Read(encryptedData, 0, CInt(dataStream.Length))

						'Convert to String
						Return Convert.ToBase64String(encryptedData, 0, encryptedData.Length)
					Finally
						theWriter.Close()
					End Try
				Finally
					encryptedStream.Close()
				End Try
			Finally
				dataStream.Close()
			End Try
		End If
	End Function

	Public Function DecryptString(ByVal AString As String) As String
		Try
			If AString = String.Empty Then
				Return AString
			Else
				Dim encryptedData() As Byte
				Dim dataStream As MemoryStream
				Dim encryptedStream As CryptoStream
				Dim strLen As Integer

				'Get the byte data
				encryptedData = Convert.FromBase64String(AString)

				Try
					dataStream = New MemoryStream
					Try
						'Create decryptor and stream
						Dim decryptor As ICryptoTransform
						decryptor = mProvider.CreateDecryptor()
						encryptedStream = New CryptoStream(dataStream, decryptor, CryptoStreamMode.Write)

						'Write the decrypted data to the memory stream
						encryptedStream.Write(encryptedData, 0, encryptedData.Length - 1)
						encryptedStream.FlushFinalBlock()

						'Position back at start
						dataStream.Position = 0

						'Determine length of decrypted string
						strLen = CInt(dataStream.Length)

						'Create area for data
						ReDim encryptedData(strLen - 1)

						'Read decrypted data to byte()
						dataStream.Read(encryptedData, 0, strLen)

						'Construct string from byte()
						Dim retStr As String

						Dim i As Integer
						For i = 0 To strLen - 1
							retStr += Chr(encryptedData(i))
						Next

						'Return result
						Return retStr
					Finally
						encryptedStream.Close()
					End Try
				Finally
					dataStream.Close()
				End Try
			End If
		Catch ex As Exception
			Return AString
		End Try
	End Function
End Class
